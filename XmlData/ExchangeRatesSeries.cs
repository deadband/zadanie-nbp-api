﻿namespace XmlData
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class ExchangeRatesSeries
    {
        [System.Xml.Serialization.XmlArrayItemAttribute("Rate", IsNullable = false)]
        public ExchangeRatesSeriesRate[] Rates { get; set; }
        public string Table { get; set; }
        public string Currency { get; set; }
        public string Code { get; set; }
    }
}