﻿namespace XmlData
{
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ExchangeRatesSeriesRate
    {
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime EffectiveDate { get; set; }
        public string No { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
    }
}