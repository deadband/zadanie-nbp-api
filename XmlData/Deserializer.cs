﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XmlData
{
    public class Deserializer
    {
        public async Task<ExchangeRatesSeries> ImportFromFile(string url)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept
                .Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(@"application/xml"));
            var xml = await httpClient.GetStringAsync(url);
            ExchangeRatesSeries table;
            var objectToDeserialize = new XmlSerializer(typeof(ExchangeRatesSeries));           
            using (var xmlFile = new StringReader(xml))
            {
                table = (ExchangeRatesSeries) objectToDeserialize.Deserialize(xmlFile);
            }
            return table;
        }
    }
}