﻿using System;
using XmlData;
using System.Linq;

namespace Logic
{
    public class RatesCalculator
    {
        private readonly Deserializer _deserializer = new Deserializer();
        private readonly ExchangeRatesSeriesRate[] _ratesTable;

        public RatesCalculator(string url)
        {           
            _ratesTable = _deserializer.ImportFromFile(url).Result.Rates;
        }

        public decimal CalculateAverageBid()
        {
            return Math.Round(_ratesTable.Select(r => r.Bid).Sum() / _ratesTable.Length, 4);
        }

        public decimal CalculateStandardDeviation()
        {
            var averageAsk = _ratesTable.Select(r => r.Ask).Sum() / _ratesTable.Length;
            var sumOfSquares = _ratesTable.Select(r => (r.Ask - averageAsk) * (r.Ask - averageAsk)).Sum();
            return Math.Round((decimal) Math.Sqrt((double) sumOfSquares / _ratesTable.Length), 4);
        }
    }
}
