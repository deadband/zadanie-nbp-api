﻿using System;
using System.Linq;
using Logic;
using System.Text.RegularExpressions;

namespace ZadanieNBPApi
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine(@"Podaj kod waluty i przedział czasowy w formacie: ""USD 2017-01-01 2017-05-23""");
            var input = Console.ReadLine();
            while (!VerifyInput(input))
            {
                Console.WriteLine(@"Podaj kod waluty i przedział czasowy w formacie: ""USD 2017-01-01 2017-05-23""");
                input = Console.ReadLine();
            }
            var url = @"http://api.nbp.pl/api/exchangerates/rates/c/" + input.ToLower().Replace(' ', '/');
            var ratesCalculator = new RatesCalculator(url);
            var bidAverage = ratesCalculator.CalculateAverageBid();
            var standardDeviation = ratesCalculator.CalculateStandardDeviation();
            Console.WriteLine($"Średni kurs kupna:                       {bidAverage}\n" +
                              $"Odchylenie standardowe kursów sprzedaży: {standardDeviation}");
        }

        private static bool VerifyInput(string input)
        {
            var availableCurrencies = new string[]
                {"USD", "AUD", "CAD", "EUR", "HUF", "CHF", "GBP", "JPY", "CZK", "DKK", "NOK", "SEK", "XDR"};

            var formatOk = Regex.IsMatch(input, @"^[a-zA-Z]{3}([ ]{1}20[01]\d{1}([-]{1}\d{2}){2}){2}$");
            var datesOk = false;
            var currencyOk = false;

            if (!formatOk)
            {
                Console.WriteLine("Niepoprawny format danych wejściowych");
                return false;
            }

            var inputSplit = input.Split(' ');
            var currency = inputSplit[0].ToUpper();
            var beginDate = DateTime.Parse(inputSplit[1]);
            var endDate = DateTime.Parse(inputSplit[2]);
            datesOk = beginDate < endDate && endDate < DateTime.Now
                                          && beginDate > DateTime.Parse("2002-01-02");
            currencyOk = availableCurrencies.Contains(currency);

            if (!datesOk) Console.WriteLine("Podano nieprawidłowy przedział czasowy.");
            if (!currencyOk) Console.WriteLine("Podano nieprawidłowy symbol waluty");

            return datesOk && currencyOk;
        }
    }
}